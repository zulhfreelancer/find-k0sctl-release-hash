#!/bin/bash
# FYI, `gh` is the official GitHub CLI

repo="k0sproject/k0sctl"
releases=$(gh release list --repo $repo | awk '{print $1}')

rm -rf ./v*

for r in $releases; do
  echo "Downloading checksums for release $r..."
  gh release download $r --pattern checksums.txt --repo $repo --dir $r
  echo "Done"
  echo
done
