#!/bin/bash

# From https://github.com/k0sproject/homebrew-tap/blob/38bfbb99f673ca8870b59ee1a01302d371d25ef1/Formula/k0sctl.rb#L12-L13
grep -R "6e19f3b63330446bc7c54189a76a87c36c958002a922f846e1e73338f8430d0a" .
grep -R "6d374710fe31b22ed182be809e96ad2dcc9912fe906f02b1f1d983c018e4325d" .

# Output:
# $ ./1-find.sh
# ./1-find.sh:grep -R "6e19f3b63330446bc7c54189a76a87c36c958002a922f846e1e73338f8430d0a" .
# ./1-find.sh:grep -R "6d374710fe31b22ed182be809e96ad2dcc9912fe906f02b1f1d983c018e4325d" .
